//Import router
const express = require('express');
const router = express.Router();
const path = require('path'); //Path

//Import Middlewares
const {signInMiddleware} = require('../middlewares/signInMiddleware');
const {signInMiddleware2} = require('../middlewares/signInMiddleware2');

router.get('/', signInMiddleware, signInMiddleware2, (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(__dirname, '../../views/sign_in.html'));
})

module.exports = router; //Export router để call main app ở file index.js