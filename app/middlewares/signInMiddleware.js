const signInMiddleware = (request, response, next) => {
  console.log('Middleware level 1');
  console.log(`Method: ${request.method} - Time: ${new Date()}`);
  next()
}

module.exports = {
  signInMiddleware
};