const signInMiddleware2 = (request, response, next) => {
  console.log('Middleware level 2');
  console.log(`URL: ${request.url}`);
  next()
}

module.exports = {
  signInMiddleware2
};